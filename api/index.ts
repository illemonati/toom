import express from "express";
import bodyParser from "body-parser";
import { uuidv4 } from "./utils";
import http from "http";
import url from "url";
import socketIo, { Client, Socket } from "socket.io";

const app = express();
const httpServer = http.createServer(app);
const io = socketIo(httpServer);

app.use(bodyParser.json());

interface Session {
    sessionId: string;
    clients: Socket[];
    offers: RTCSessionDescriptionInit[];
    answers: RTCSessionDescriptionInit[];
    candidates: any[];
}

const sessions: { [key: string]: Session } = {};

app.get("/create-session", (req, res) => {
    const sessionId = uuidv4();
    sessions[sessionId] = {
        sessionId,
        clients: [],
        offers: [],
        answers: [],
        candidates: []
    };
    res.json({
        sessionId
    });
});

io.on("connection", socket => {
    socket.emit("connected");
    socket.on("join-session", async sessionId => {
        if (sessions[sessionId]) {
            sessions[sessionId].clients.push(socket);
            socket.emit("session-joined", {
                isCaller: sessions[sessionId].clients.length === 1
            });
        }
    });
    socket.on("offer", async msg => {
        const { offer, sessionId } = msg;
        if (!sessions[sessionId]) return;
        sessions[sessionId].offers.push(offer);
        for (const client of sessions[sessionId].clients) {
            client.emit("offer", offer);
        }
    });
    socket.on("answer", async msg => {
        const { answer, sessionId } = msg;
        console.log("got answer");
        if (!sessions[sessionId]) return;
        sessions[sessionId].answers.push(answer);
        console.log("relaying answer");
        for (const client of sessions[sessionId].clients) {
            client.emit("answer", answer);
        }
    });
    socket.on("candidate", async msg => {
        const { candidate, sessionId } = msg;
        if (!sessions[sessionId]) return;
        sessions[sessionId].candidates.push(candidate);
        for (const client of sessions[sessionId].clients) {
            client.emit("candidate", candidate);
        }
    });
    socket.on("request-old", async sessionId => {
        if (!sessions[sessionId]) return;
        console.log("old-request");
        for (const offer of sessions[sessionId].offers) {
            socket.emit("offer", offer);
        }
        // for (const answer of sessions[sessionId].answers) {
        //     socket.emit("answer", answer);
        // }
        for (const candidate of sessions[sessionId].candidates) {
            socket.emit("candidate", candidate);
        }
    });
});

export default {
    path: "/api/",
    handler: httpServer
};
